# Calculatrice en Python

Ce projet est une calculatrice simple en Python, développée dans le cadre d'un projet d'équipe. Chaque membre est responsable d'une opération mathématique spécifique.

## Fonctionnalités

- Addition
- Soustraction
- Multiplication
- Division

## Comment exécuter le programme

Pour exécuter la calculatrice, assurez-vous d'avoir Python installé sur votre système. Ensuite, suivez ces étapes :

1. Clonez le dépôt sur votre ordinateur.
2. Assurez-vous d'être dans la branche integration_calculatrice.
3. Lancez le fichier principal calculatrice.py pour démarrer l'application.

## Structure du projet

- addition.py : Contient la logique pour l'opération d'addition.
- soustraction.py : Contient la logique pour l'opération de soustraction.
- multiplication.py : Contient la logique pour l'opération de multiplication.
- division.py : Contient la logique pour l'opération de division.
- calculatrice.py : Fichier principal qui appelle les différentes opérations mathématiques.

## Collaborateurs

- Étudiant 1 HONFO Dine (Addition)
- Étudiant 2 SEMASSOU Eurydice(Soustraction)
- Étudiant 3 Charles YERIMA (Multiplication)
- Étudiant 4: AHOUANSOU Horelle (Division)
- Etudiant 5: LODJOU Pamela (Responsable)

